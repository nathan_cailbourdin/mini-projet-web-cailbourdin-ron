<!DOCTYPE html>
<html>
<head>
  <title>La Cave aux Bouteilles</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="styleCave.css">
  <link href="https://fonts.googleapis.com/css?family=Smythe" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
</head>
<body>
  <header>
    <h1 class='subTitle'>Notre catalogue</h1>
    <button><a href="./cave.php">Vers la cave</a></button>
    <button><a href="./index.php">Vers l'index</a></button>
  </header>
  <div>
    <?php
      require("connect.php");
      $dsn="mysql:dbname=".BASE.";host=".SERVER;
        try{
          $connexion=new PDO($dsn,USER,PASSWD);
        }
        catch(PDOException $e){
          printf("Échec de la connexion : %s\n", $e->getMessage());
          exit();
        }
        #Récupérer toutes les productions d'un catalogue
      $sql="select * from CATALOGUER natural join PRODUCTION natural join DOMAINE natural join CATALOGUE where idCat = :idCat";
      $stmt=$connexion->prepare($sql);
      $valeur = 1;
      $stmt->bindParam(':idCat',$valeur);
      $stmt->execute();
      echo '<div id="catalogue">';
      foreach ($stmt as $result) {
          echo '<section class="sl">';
          echo '<img class="imgbout" src="'.$result['url'].'">';;
          echo '<ul>';
          echo "<li>".$result['typeP']."</li>";
          echo "<li>".$result['degre']."%"."</li>";
          echo "<li>".$result['nomDomaine']."</li>";
          echo "<li>".'Annee '.$result['annee']."</li>";
          echo "<li>".'Couleur '.$result['couleur']."</li>";

          $sql2="select count(*) as nb from RANGER natural join BOUTEILLE natural join PRODUCTION where idProd = :idProd";
          $stmt2=$connexion->prepare($sql2);
          $stmt2->bindParam(':idProd',$result['idProd']);
          $stmt2->execute();
          $nombre = 0;
          foreach ($stmt2 as $result2) {
            $nombre = $result2['nb'];
          }
          echo "<li>Il en reste ".$nombre."</li>";
          echo '</section>';
      }
      echo "</div>";

    ?>
  </div>
</body>
</html>
