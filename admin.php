<!doctype html>
  <html>
    <?php
    require_once("testadmin.php");
    if(isConnected() == 0){
      header('Location: connexion.php');
    }
    ?>
    <head>
      <title>
      La Cave aux Bouteilles
      </title>
     <meta charset="utf-8">
     <link rel="stylesheet" href="styleadmin.css">
     <link href="https://fonts.googleapis.com/css?family=Smythe" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    </head>
    <body>
      <h2>Se connecter</h2>
        <form action="login.php" method="POST" autocomplete="off">
        <div class="formulaire"><p>Pseudonyme : </p><input type ="text" name="pseudo" required>
        <p>Mot de passe : </p><input type ="password" name="mdp" required></div>
        <div class ="bas"><input class="bouton" type=submit value ="Se connecter"></div>
      </form>
  </body>
</html>
