<!doctype html>
  <html>
    <head>
      <title>
      La Cave aux Bouteilles
      </title>
     <meta charset="utf-8">
     <link rel="stylesheet" href="styleconnexion.css">
     <link href="https://fonts.googleapis.com/css?family=Smythe" rel="stylesheet">
     <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
    </head>
    <body>
      <?php
      require_once("testadmin.php");
      if(isConnected() > 0){
        echo "<h1>Vous n'êtes pas connecté en tant qu'administrateur. Veuillez vous connecter avant de pouvoir utiliser la page d'administration du site</h1>";
        echo "<div class = 'pasconnec'><button><a href='./index.php'>Retour à l'index</a></button></div>";
        exit();
      }
      ?>
      <div class ="haut">
      <div class ="retour"><a href='./index.php'>Retour à l'index</a></div>
      <h1>PAGE ADMINISTRATEUR</h1>
      </div>
      <div class ="centre">
        <div class = "partiegauche">
        <h2>Ajouter une bouteille au catalogue</h2>
        <form action="addB.php" method="GET" autocomplete="off">
          <h3>Caractéristiques de la bouteille</h3>
          <div class = "carac">
          <div class = "info"><p class ="texteinfo">Type de production : </p><input type ="text" name="typeP" required placeholder="Entrez le nom de la bouteille (ex: Vin Rouge)"></div>
          <div class = "info"><p class ="texteinfo">Annee : </p><input type ="text" name="annee" pattern="[0-9]{4}" required placeholder="Entrez l'année de production"></div>
          <div class = "info"><p class ="texteinfo">Pourcentage : </p><input type ="text" name="degre" pattern="[0-9]{2}.[0-9]{2}" required placeholder="Entrez le degré sous la forme : 00.00"></div>
          <div class = "info"><p class ="texteinfo">Couleur : </p><input type ="text" name="couleur" required placeholder="Entrez la couleur"></div>
          <div class = "info"><p class ="texteinfo">Photo : </p><input type ="text" name="url" required placeholder="Donnez l'URL de la Photo"></div>
          </div>
          <h3>Caractéristiques du domaine</h3>
          <input class ="minibouton" type="radio" name="dom_existe" value="nouveau" checked> Nouveau domaine ?
          <div class = "info"><p class ="texteinfo">Nom Domaine : </p><input type ="text" name="nomDomaine" placeholder="Entrez le nom du domaine"></div>
          <div class = "info"><p class ="texteinfo">Pays : </p><input type ="text" name="pays" placeholder="Entrez le pays du domaine"></div>
          <div class = "info"><p class ="texteinfo">Region: </p><input type ="text" name="region" placeholder="Entrez le region du domaine"></div>
          <div class = "domaineex"><input class ="minibouton" type="radio" name="dom_existe" value="existant"> Domaine déja existant ?</div>
          <?php
          require_once("connect.php");
          $dsn="mysql:dbname=".BASE.";host=".SERVER;
            try{
              $connexion=new PDO($dsn,USER,PASSWD);
            }
            catch(PDOException $e){
              printf("Échec de la connexion : %s\n", $e->getMessage());
              exit();
            }
              $sql="select * from DOMAINE";
              $stmt=$connexion->prepare($sql);
              $stmt->execute();
              echo "<select name='dom'>";
              foreach ($stmt as $result) {
                echo "<option value=".$result["idDomaine"]."> ".$result['nomDomaine']." de ".$result['pays']." ".$result['region']."<br>";
              }
              echo "</select>";
           ?>
           <br>
          <div class ="validation"><input class="bouton" type=submit value ="Ajouter cette bouteille au catalogue"></div>
        </form>
        </div>
        <div class = "partiedroite">
        <h2>Ajouter une bouteille a la cave</h2>
        <form action="addC.php" method="GET" autocomplete="off">
        <p class ="texteinfo">Production :</p>
          <?php
          require_once("connect.php");
          $dsn="mysql:dbname=".BASE.";host=".SERVER;
            try{
              $connexion=new PDO($dsn,USER,PASSWD);
            }
            catch(PDOException $e){
              printf("Échec de la connexion : %s\n", $e->getMessage());
              exit();
            }
          $sql4="select * from PRODUCTION natural join DOMAINE";
          $stmt4=$connexion->prepare($sql4);
          $stmt4->execute();
          echo "<select name='idProd' required>";
          echo "<option style='display:none' disabled selected value>Cliquez pour séléctionner une bouteille</option>";
          foreach ($stmt4 as $result) {
            echo "<option value=".$result["idProd"]."> ".$result['typeP']." de ".$result['annee']. " - ".$result['nomDomaine']."<br>";
          }
          echo "</select>";
          ?>
          <div class = "info"><p class ="texteinfo">Volume : </p><input type ="number" name="volume" placeholder="Entrez le volume (en mL)" pattern="[0-9]+" required></div>
          <div class = "info"><p class ="texteinfo">Prix : </p><input type ="text" name="prix" placeholder="Entrez le prix (en €)" required></div>
          <div class = "info"><p class ="texteinfo">Cellule : </p><input type ="number" name="cellule" placeholder="Entrez le numéro de la cellule" required></div>
          <div class ="validation"><input class="bouton" type=submit value ="Ajouter cette bouteille à la cave"></div>
        </form>

        <h2>Supprimer une bouteille de la cave</h2>
        <form action="removeC.php" method="GET" autocomplete="off">
          <div class = "info"><p class ="texteinfo">Bouteille à retirer : </p>
          <?php
          require_once("connect.php");
          $dsn="mysql:dbname=".BASE.";host=".SERVER;
            try{
              $connexion=new PDO($dsn,USER,PASSWD);
            }
            catch(PDOException $e){
              printf("Échec de la connexion : %s\n", $e->getMessage());
              exit();
            }
          $sql4="select * from RANGER natural join BOUTEILLE natural join PRODUCTION natural join DOMAINE order by numCel";
          $stmt4=$connexion->prepare($sql4);
          $stmt4->execute();
          echo "<select name='removeBout' required>";
          echo "<option style='display:none' disabled selected value>Cliquez pour séléctionner une bouteille</option>";
          foreach ($stmt4 as $result) {
            echo "<option value=".$result["idBout"]."> Cellule ".$result['numCel']." : ".$result['typeP']." de ".$result['annee']." à ".$result['prix']."€ du ".$result['nomDomaine']."<br>";
          }
          echo "</select>";
          ?>
            </div>
          <div class ="validation"><input class="bouton" type=submit value ="Enlever cette bouteille de la cave"></div>
        </form>
      </div>
    </div>
    <footer>Site par Nathan Cailbourdin et Thomas Ron - Module Web Serveur</footer>
  </body>
</html>
