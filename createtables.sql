drop table if exists RANGER;
drop table if exists CATALOGUER;
drop table if exists BOUTEILLE;
drop table if exists CAVE;
drop table if exists PRODUCTION;
drop table if exists CATALOGUE;
drop table if exists DOMAINE;
drop table if exists PROPRIETAIRE;

CREATE TABLE CAVE (
  idCave decimal(4,0) NOT NULL,
  idProp decimal(4,0) NOT NULL,
  PRIMARY KEY (idCave)
);

CREATE TABLE BOUTEILLE (
  idBout decimal(4,0) NOT NULL,
  idProd decimal(4,0) NOT NULL,
  volume decimal(4,0) NOT NULL,
  prix decimal(7,2) NOT NULL,
  PRIMARY KEY (idBout)
);

CREATE TABLE RANGER(
	idCave decimal(4,0) NOT NULL,
	idBout decimal(4,0) NOT NULL,
	numCel decimal(4,0),
	PRIMARY KEY (idCave,idBout,numCel)
);

CREATE TABLE CATALOGUE(
	idCat decimal(4,0) NOT NULL,
	idProp decimal(4,0) NOT NULL,
	PRIMARY KEY (idCat)
);

CREATE TABLE PRODUCTION(
	idProd decimal(4,0) NOT NULL,
	annee decimal(4,0) NOT NULL,
	typeP varchar(30) NOT NULL,
	idDomaine decimal(4,0) NOT NULL,
	couleur varchar(20) NOT NULL,
	degre decimal(4,2) NOT NULL,
	url text NOT NULL,
	PRIMARY KEY (idProd)
);

CREATE TABLE CATALOGUER(
	idCat decimal(4,0) NOT NULL,
	idProd decimal(4,0) NOT NULL,
	PRIMARY KEY (idCat,idProd)
);

CREATE TABLE DOMAINE(
  idDomaine decimal(4,0) NOT NULL,
  nomDomaine varchar(50) NOT NULL,
  pays varchar(20) NOT NULL,
  region varchar(30) NOT NULL,
  PRIMARY KEY (idDomaine)
);

CREATE TABLE PROPRIETAIRE (
  idProp decimal(4,0) NOT NULL,
  nomProp varchar(20) NOT NULL,
  prenomProp varchar(20) NOT NULL,
  mdpProp varchar(20) NOT NULL,
  pseudo varchar(20) NOT NULL,
  PRIMARY KEY (idProp)
);

ALTER TABLE PRODUCTION ADD UNIQUE (annee,typeP,idDomaine);
ALTER TABLE CAVE ADD FOREIGN KEY (idProp) REFERENCES PROPRIETAIRE (idProp);
ALTER TABLE RANGER ADD UNIQUE (idCave,numCel);
ALTER TABLE RANGER ADD FOREIGN KEY (idCave) REFERENCES CAVE (idCave);
ALTER TABLE RANGER ADD FOREIGN KEY (idBout) REFERENCES BOUTEILLE (idBout);
ALTER TABLE BOUTEILLE ADD FOREIGN KEY (idProd) REFERENCES PRODUCTION (idProd);
ALTER TABLE CATALOGUE ADD FOREIGN KEY (idProp) REFERENCES PROPRIETAIRE (idProp);
ALTER TABLE CATALOGUER ADD FOREIGN KEY (idProd) REFERENCES PRODUCTION (idProd);
ALTER TABLE PRODUCTION ADD FOREIGN KEY (idDomaine) REFERENCES DOMAINE (idDomaine);
