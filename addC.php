<!doctype html>
  <html>
    <head>
      <title>
      La Cave aux Bouteilles
      </title>
     <meta charset="utf-8">
    </head>
    <body>
      <?php
        require("connect.php");
        $dsn="mysql:dbname=".BASE.";host=".SERVER;
          try{
            $connexion=new PDO($dsn,USER,PASSWD);
          }
          catch(PDOException $e){
            printf("Échec de la connexion : %s\n", $e->getMessage());
            exit();
          }
            $sql="select max(idBout) as max from BOUTEILLE";
            $stmt=$connexion->prepare($sql);
            $stmt->execute();
            foreach ($stmt as $result) {
              $newbout = $result['max']+1;
            }
          #Mettre une production dans une cave
          $sql2="insert into BOUTEILLE values (:idBout,:idProd,:volume,:prix)";
          $stmt2=$connexion->prepare($sql2);
          $stmt2->bindParam(':idBout',$newbout);
          $stmt2->bindParam(':idProd',$_GET['idProd']);
          $stmt2->bindParam(':volume',$_GET['volume']);
          $stmt2->bindParam(':prix',$_GET['prix']);
          $stmt2->execute();

          $sql3="insert into RANGER values (:idCave,:idBout,:numCel)";
          $valeur = 1;
          $stmt3=$connexion->prepare($sql3);
          $stmt3->bindParam(':idCave',$valeur);
          $stmt3->bindParam(':idBout',$newbout);
          $stmt3->bindParam(':numCel',$_GET['cellule']);
          $stmt3->execute();
          header('Location: cave.php');
          exit();
          ?>
    </body>
  </html>
