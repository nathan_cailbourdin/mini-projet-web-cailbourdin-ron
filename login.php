<!doctype html>
  <html>
    <head>
      <title>
      La Cave aux Bouteilles
      </title>
     <meta charset="utf-8">
    </head>
    <body>
      <?php
        require_once("connect.php");
        $dsn="mysql:dbname=".BASE.";host=".SERVER;
          try{
            $connexion=new PDO($dsn,USER,PASSWD);
          }
          catch(PDOException $e){
            printf("Échec de la connexion : %s\n", $e->getMessage());
            exit();
          }

          $sql="select mdpProp from PROPRIETAIRE where pseudo = :pseudo";
          $stmt=$connexion->prepare($sql);
          $stmt->bindParam(':pseudo',$_POST['pseudo']);
          $stmt->execute();
          foreach ($stmt as $result) {
            $res = $result['mdpProp'];
          }
          if($res == $_POST['mdp']){
            session_start();
            $_SESSION['pseudo'] = $_POST['pseudo'];
            $_SESSION['mdp'] = $_POST['mdp'];
            header('Location: connexion.php');
            exit();
          }
          else{
            header('Location: admin.php');
            exit();
          }
      ?>
  </body>
</html>
