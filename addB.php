<!doctype html>
  <html>
    <head>
      <title>
      La Cave aux Bouteilles
      </title>
     <meta charset="utf-8">
    </head>
    <body>
      <?php
        require_once("connect.php");
        $dsn="mysql:dbname=".BASE.";host=".SERVER;
          try{
            $connexion=new PDO($dsn,USER,PASSWD);
          }
          catch(PDOException $e){
            printf("Échec de la connexion : %s\n", $e->getMessage());
            exit();
          }

        #Ajouter une production dans le catalogue
        $sql="select max(idProd) as max from PRODUCTION";
        $stmt=$connexion->prepare($sql);
        $stmt->execute();
        foreach ($stmt as $result) {
          $newprod = $result['max']+1;
        }

        #Ajout d'un domaine s'il n'existe pas déja
        if($_GET['dom_existe']=="nouveau"){

          $sql4="select max(idDomaine) as max from DOMAINE";
          $stmt4=$connexion->prepare($sql4);
          $stmt4->execute();
          foreach ($stmt4 as $result) {
            $newdom = $result['max']+1;
          }

          $sql5="insert into DOMAINE(idDomaine,nomDomaine,pays,region) values (:idDomaine,:nomDomaine,:pays,:region)";
          $stmt5=$connexion->prepare($sql5);
          $stmt5->bindParam(':idDomaine',$newdom);
          $stmt5->bindParam(':nomDomaine',$_GET['nomDomaine']);
          $stmt5->bindParam(':pays',$_GET['pays']);
          $stmt5->bindParam(':region',$_GET['region']);
          $stmt5->execute();
        }

        if($_GET['dom_existe']=="existant"){
          $newdom = $_GET['dom'];
        }
        if($newprod == 0 || $_GET['annee'] == 0 || $_GET['typeP'] == "" || $_GET['url'] == "" || $newdom == 0 || $_GET['couleur'] == "" || $_GET['degre'] < 0){
          header('Location: connexion.php');
          exit();
        }

       	$sql2="insert into PRODUCTION(idProd,annee,typeP,idDomaine,couleur,degre,url) values (:idProd,:annee,:typeP,:idDomaine,:couleur,:degre,:url)";
        $stmt2=$connexion->prepare($sql2);
        $stmt2->bindParam(':idProd',$newprod);
        $stmt2->bindParam(':annee',$_GET['annee']);
        $stmt2->bindParam(':typeP',$_GET['typeP']);
        $stmt2->bindParam(':url',$_GET['url']);
        $stmt2->bindParam(':idDomaine',$newdom);
        $stmt2->bindParam(':couleur',$_GET['couleur']);
        $stmt2->bindParam(':degre',$_GET['degre']);
        $stmt2->execute();

        $sql3="insert into CATALOGUER values (:idCat,:idProd)";
        $valeur = 1;
        $stmt3=$connexion->prepare($sql3);
        $stmt3->bindParam(':idCat',$valeur);
      	$stmt3->bindParam(':idProd',$newprod);
        $stmt3->execute();
        header('Location: catalogue.php');
      ?>
  </body>
</html>
