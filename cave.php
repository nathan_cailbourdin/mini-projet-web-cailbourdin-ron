<!DOCTYPE html>
<html>
<head>
  <title>La Cave aux Bouteilles</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="styleCave.css">
  <link href="https://fonts.googleapis.com/css?family=Smythe" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
</head>
<body>
  <header>
    <h1 class='subTitle'>Notre cave</h1>
    <button><a href="./catalogue.php">Vers le catalogue</a></button>
    <button><a href="./index.php">Vers l'index</a></button>
  </header>
  <div>
    <?php
      require("connect.php");
      $dsn="mysql:dbname=".BASE.";host=".SERVER;
        try{
          $connexion=new PDO($dsn,USER,PASSWD);
        }
        catch(PDOException $e){
          printf("Échec de la connexion : %s\n", $e->getMessage());
          exit();
        }
      #Exemple requête préparée

      #Récupérer toutes les bouteilles d'une certaine cave
      $sql="select * from RANGER natural join BOUTEILLE natural join PRODUCTION natural join DOMAINE natural join CAVE where idCave=:idCave order by numCel";
      $stmt=$connexion->prepare($sql);
      $valeur = 1;
      $stmt->bindParam(':idCave',$valeur);
      $stmt->execute();
      echo '<div id="cave">';
      foreach ($stmt as $result) {
        echo "<section class ='sl'>";
        echo "<p>Cellule n°".$result['numCel']."</p>";
        echo '<img class="imgbout" src="'.$result['url'].'">';
        echo "<ul>";
        echo "<li class ='nom'>".$result['typeP']."<li>";
        echo "<li>".$result['nomDomaine']."<li>";
        echo "<li>Annee : ".$result['annee']."<li>";
        echo "<li>Prix : ".$result['prix']."€<li>";
        echo "<li>Vol : ".$result['volume']."ml</p>";
        echo "</ul>";
        echo "</section>";

      }
      echo "</ul>";
      ?>
  </div>

</body>
