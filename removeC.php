<!doctype html>
  <html>
    <head>
      <title>
      La Cave aux Bouteilles
      </title>
     <meta charset="utf-8">
    </head>
    <body>
      <?php
        require_once("connect.php");
        $dsn="mysql:dbname=".BASE.";host=".SERVER;
          try{
            $connexion=new PDO($dsn,USER,PASSWD);
          }
          catch(PDOException $e){
            printf("Échec de la connexion : %s\n", $e->getMessage());
            exit();
          }

          #Enlever la bouteille
          $sql2="delete from RANGER where idBout = :idBout";
          $stmt2=$connexion->prepare($sql2);
          $stmt2->bindParam(':idBout',$_GET['removeBout']);
          $stmt2->execute();

          $sql3="delete from BOUTEILLE where idBout = :idBout";
          $stmt3=$connexion->prepare($sql3);
          $stmt3->bindParam(':idBout',$_GET['removeBout']);
          $stmt3->execute();
          header('Location: cave.php');
          exit();
          ?>
    </body>
  </html>
