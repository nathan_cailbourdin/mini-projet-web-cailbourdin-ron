<!DOCTYPE html>
<html>
<head>
  <title>La Cave aux Bouteilles</title>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="styleindex.css">
  <link href="https://fonts.googleapis.com/css?family=Smythe" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Great+Vibes" rel="stylesheet">
</head>
<body>
  <div class="titre">
        La Cave aux Bouteilles
  </div>
  <div class="legende">
        Pour les alcoliques
  </div>
  <div class="liens">
        <button><a href="./cave.php">Vers la cave</a></button>
        <button><a href="./catalogue.php">Vers le catalogue</a></button>
        <button><a href="./admin.php">accès administrateur</a></button>
  </div>
</body>
</html>
